<?php

class Articles {

    protected $ci;
    protected $model;
    public $limit = 5;

    function __construct() {
        $this->ci = & get_instance();
        $this->ci->load->model('articles_model');
        $this->model = $this->ci->articles_model;
    }

    public function getFrontArticles() {
        return self::getArticles($this->model->articles_model->getArticles(0, $this->limit, 'date_desc', 'Live'));
    }

    public function getArticlesByPlace($place, $limit = 5) {
        return self::getArticles($this->model->articles_model->getArticlesByPlace($place, $limit));
    }

    public function getArticlesByCatID($catid, $limit, $offset) {
        return self::getArticles($this->model->articles_model->getArticlesByCatID($catid, $limit, $offset));
    }

    private function getArticles($article_data) {
        if (empty($article_data))
            return array();
        foreach ($article_data as &$data) {
            $data->id = $data->id;
            $data->summary = $data->summary;
            $data->title = $data->title;
            $data->image = $this->getImage($data->id);
        }
        return $article_data;
    }

    public function getImage($id) {
        return $this->model->articles_model->getImageOfArticle($id);
    }

    public function getUserByID($id = 0) {
        return $this->model->getUserByID($id);
    }

    public function getUserByUrl($url) {
        return self::UserInfo($this->model->getUserByUrl($url));
    }

    public static function UserInfo($user) {
        $user->id = $user->id;
        $user->name = short_name_format($user->name);
        $user->avatar = $user->avatar;
        $user->seo_url = $user->seo_url;
        $user->created_at = $user->created_at;
        return $user;
    }

    public function getUserComments($id) {
        return self::Comments($this->model->getUserComments($id));
    }

    function setLogin($u) {
        $this->ci->session->set_userdata($u);
        return;
    }

    function getLatestComments($f_id = 0) {
        return $this->model->getLatestComments($f_id);
    }

    function getSingle($f_id = 0) {
        return $this->model->getEntry($f_id);
    }

    function getComments($f_id = 0) {
        return $this->model->getLatestComments($f_id);
    }

    public static function Comments($coment_data) {
        if (empty($coment_data))
            return array();
        foreach ($coment_data as &$data) {
            $data->created_at = relative_time($data->created_at);
            // path
            $data->title = $data->entry_title;
            $data->media_value = $data->media_value;
            $data->comment = $data->comment;
        }
        return $coment_data;
    }

}