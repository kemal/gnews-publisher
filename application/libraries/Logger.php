<?php

class Logger {
    
    public static function info($action, $fb_uid = '') {
        try {
            $ci = & get_instance();
            $log = new stdClass();
            $log->log_action = $action;
            $log->user_ip = get_cf_ip();
            $log->user_agent = $ci->input->user_agent();
            $log->page_uri = $_SERVER['REQUEST_URI'];
            $log->created_at = time();
            if(!empty ($fb_uid)) {
                $log->fb_uid = $fb_uid;
            }
            $ci->db->insert('app_log', $log);
        } catch ( Exception $e ) {
        }
    }

    public static function error($msg) {
        try {
            $ci = & get_instance();
            $log = new stdClass();
            $log->log_detail = $msg;
            $log->user_ip = get_cf_ip();
            $log->user_agent = $ci->input->user_agent();
            $log->page_uri = $_SERVER['REQUEST_URI'];
            $log->created_at = time();
            $ci->db->insert('error_log', $log);
        } catch ( Exception $e ) {
        }
    }
    
 
    
    public static function php_error($message, $filepath, $line, $severity) {
        try {
            $ci = & get_instance();
            $log = new stdClass();
            $log->error_desc = $message;
            $log->error_file = $filepath . ' (' . $line . ')';
            $log->error_level = $severity;
            $log->user_ip = get_cf_ip();
            $log->user_agent = $ci->input->user_agent();
            $log->page_uri = $_SERVER['REQUEST_URI'];
            $log->created_at = time();
            $ci->db->insert('php_error_log', $log);
        } catch ( Exception $e ) {
        }
    }

}