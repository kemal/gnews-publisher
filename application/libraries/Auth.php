<?php

class Auth {

	protected $ci;
	protected $model;

	function __construct() {
		$this->ci = & get_instance();
		$this->ci->load->model('users_model');
		$this->model = $this->ci->users_model;
		$this->ci->load->library('session');
	}

	function check_access() {
		$user = $this->ci->session->userdata('id');
		$rst = $this->model->users_model->checkLoginByID($user);
		if ($rst != 1) {
			redirect('admin/login', 'refresh');
		}
	}
	function isAllowed($controller = ''){
		$controllers = Array(
				'Gallery'=>Array('Admin','Author','Editor'),
				'Articles'=>Array('Admin','Author','Editor'),
				'RSS'=>Array('Admin','Editor'),
				'User'=>Array('Admin'),
				'Category'=>Array('Admin','Editor'),
				'Categories'=>Array('Admin','Editor')
		);
		$level = $this->ci->session->userdata('level');
		if (!in_array($level, $controllers[$controller])) redirect('admin/can_not_access', 'refresh');;

	}

}