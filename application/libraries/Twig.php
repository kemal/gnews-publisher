<?php

class Twig {

    private $CI;
    private $_twig;
    private $_template_dir;
    private $_cache_dir;
    private $_cache_on;
    private $_debug;
    private $_auto_reload;

    /**
     * Constructor
     *
     */
    function __construct() {
        $this->CI = & get_instance();
        $this->CI->config->load('twig');

        ini_set('include_path', ini_get('include_path') . PATH_SEPARATOR . APPPATH . 'libraries/Twig');
        require_once (string) "Autoloader" . EXT;
        require_once (string) "Extensions/Autoloader" . EXT;

        log_message('debug', "Twig Autoloader Loaded");

        Twig_Autoloader::register();
        Twig_Extensions_Autoloader::register();

        $this->_template_dir = $this->CI->config->item('template_dir');
        $this->_cache_dir = $this->CI->config->item('cache_dir');
        $this->_cache_on = $this->CI->config->item('cache_on');
        $this->_debug = $this->CI->config->item('debug');
        $this->_auto_reload = $this->CI->config->item('auto_reload');
        $loader = new Twig_Loader_Filesystem($this->_template_dir);        
        $opt = array();
        $opt['debug'] = $this->_debug;
        $opt['auto_reload'] = $this->_auto_reload;
        
        if($this->_cache_on) {
            $opt['cache'] = $this->_cache_dir;
        }

        $this->_twig = new Twig_Environment($loader, $opt);
        $this->_twig->addExtension(new Twig_Extensions_Extension_Text());
    }

    public function add_function($name) {
        $this->_twig->addFunction($name, new Twig_Function_Function($name));
    }

    public function render($template, $data = array()) {
        $template = $this->_twig->loadTemplate($template);
        return $template->render($data);
    }

    public function display($template, $data = array()) {
        $template = $this->_twig->loadTemplate($template);
        /* elapsed_time and memory_usage */
        $data['elapsed_time'] = $this->CI->benchmark->elapsed_time('total_execution_time_start', 'total_execution_time_end');
        $memory = (!function_exists('memory_get_usage')) ? '0' : round(memory_get_usage() / 1024 / 1024, 2) . 'MB';
        $data['memory_usage'] = $memory;
        $template->display($data);
    }

}