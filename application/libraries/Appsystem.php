<?php
class Appsystem {
    protected $ci;
    protected $model;
    function __construct() {
        $this->ci = & get_instance();
        $this->ci->load->model('system_model');
        $this->model = $this->ci->system_model;
    }
     
    public function getSystemSetting(){
        return $this->Settings($this->model->getSystemSetting());
    } 
    public function Settings($_data) {
        
        if (empty($_data)) return array();
        foreach ($_data as $value) {
            $data[$value->name] = $value->value;
        }
        return $data;
    }
}