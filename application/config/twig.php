<?php

$config['debug'] = false;
$config['template_dir'] = APPPATH.'views';
$config['cache_dir'] = APPPATH.'cache/twig';
$config['cache_on'] = false;
$config['auto_reload'] = true;