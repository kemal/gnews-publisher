<?php

class Autoloader {
    
    protected static $_instance;
    protected $_searchDirs = array();

    public static function getInstance()
    {
        if (null === self::$_instance) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }
    
    public function addClassPath($dir) {
        $realPath = realpath($dir);
        if ($realPath !== false) {
            $this->_searchDirs[] = $dir;
        }
        return $this;
    }
    
    public function loadClass($className) {
        $classFileName = DIRECTORY_SEPARATOR
                        . str_replace('_', DIRECTORY_SEPARATOR, $className)
                        . '.php';
        foreach ($this->_searchDirs as $dir) {
            $fullFileName = $dir . $classFileName;
            if (is_file($fullFileName) && is_readable($fullFileName)) {
                require_once($fullFileName);
                if (class_exists($className)) {
                    return;
                }
            }
        }
    }
    
    public function register() {
        spl_autoload_register(array(__CLASS__, 'loadClass'));
    }
}

Autoloader::getInstance()
        ->addClassPath(realpath(APPPATH) . '/libraries')
        ->register();