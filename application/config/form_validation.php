<?php

$config = array(
    'admin/user/add' => array(
        array(
            'field' => 'email',
            'label' => 'Email',
            'rules' => 'required|valid_email'
        ),
        array(
            'field' => 'name',
            'label' => 'Name',
            'rules' => 'required'
        ),
        array(
            'field' => 'password',
            'label' => 'Password',
            'rules' => 'required|matches[passwordcon]|min_length[5]|max_length[12]'
        ),
        array(
            'field' => 'passwordcon',
            'label' => 'Password Confirmation',
            'rules' => 'required'
        ),
    ),
    'category' => array(
        array(
            'field' => 'name',
            'label' => 'Name',
            'rules' => 'trim|required'
        )
    ),
    'admin/rss/insert' => array(
        array(
            'field' => 'name',
            'label' => 'Name',
            'rules' => 'trim|required'
        )
    ),
    'admin/rss/edit' => array(
        array(
            'field' => 'name',
            'label' => 'Name',
            'rules' => 'trim|required'
        )
    ),
    'admin/login' => array(
        array(
            'field' => 'email',
            'label' => 'Email',
            'rules' => 'valid_email|required'
        ),
        array(
            'field' => 'password',
            'label' => 'Password',
            'rules' => 'required'
        )
    )
);