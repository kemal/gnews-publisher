<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "default/home";
$route['category/(:any)'] = "default/category";
$route['article/(:any)'] = "default/article";
$route['gallery/(:any)'] = "default/article/gallery";
$route['video/(:any)'] = "default/article/video";
$route['404_override'] = '';
$route['sitemap'] = "panel/sitemap";



/*
 * ADMIN ROUTING
 */
$route['admin'] = 'panel/home';
$route['admin/login'] = 'panel/login';
$route['admin/can_not_access'] = 'panel/login/failed';
$route['admin/logout'] = 'panel/login/logout';

$route['admin/articles'] = 'panel/article';
$route['admin/articles/(:any)'] = 'panel/article';
$route['admin/newarticle'] = 'panel/article/newarticle';
$route['admin/article/edit/(:any)'] = 'panel/article/edit';
$route['admin/article/view/(:any)'] = 'panel/article/view';
$route['admin/update'] = 'panel/article/update';
$route['admin/categories'] = 'panel/categories';
$route['admin/users'] = 'panel/users';
$route['admin/user/add'] = 'panel/users/add';
$route['admin/user/update'] = 'panel/users/update';
$route['admin/user/edit/(:any)'] = 'panel/users/edit';
$route['admin/category/(:any)'] = 'panel/categories';
$route['admin/gallery/(:any)'] = 'panel/gallery';
$route['admin/gallery/photos/(:any)'] = 'panel/gallery/photos';
$route['admin/photo/edit'] = 'panel/gallery/update';
$route['admin/photo/delete/(:any)'] = 'panel/gallery/delete';
$route['admin/video/delete/(:any)'] = 'panel/gallery/vdelete';
$route['admin/theme/(:any)'] = 'panel/theme';
$route['admin/rss'] = 'panel/rss_reader';
$route['admin/rss/download/(:any)'] = 'panel/rss_reader/download';
$route['admin/rss/edit(:any)'] = 'panel/rss_reader/editrss';
$route['admin/rss/insert'] = 'panel/rss_reader/insertrss';

 
/* End of file routes.php */
/* Location: ./application/config/routes.php */