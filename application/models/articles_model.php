<?php

class Articles_model extends CI_Model {

    public function openLastArticle() {
        $this->db->select('*')
                ->from('articles')
                ->order_by('id', 'asc')
                ->where('status', 'Draft');
        $last = $this->db->get()->row();
        if (count($last) == 0) {
            $this->db->insert('articles', Array('status' => 'Draft'));
            $this->db->select('*')
                    ->from('articles')
                    ->where('status', 'Draft')
                    ->order_by('id', 'desc');
            $last = $this->db->get()->row();
        }
        return $last;
    }

    public function getModules() {
        $this->db->select('*');
        $this->db->from('page_parts');
        return $this->db->get()->result();
    }

    public function insertnew($article) {
        $this->db->insert('articles', $article);
        return $this->db->insert_id();
    }

    public function addImage2Gallery($filename, $id, $dirpath) {
        $new = Array('gallery_type' => 'image', 'filename' => $filename, 'article_id' => $id, 'dirpath' => $dirpath);
        $this->db->insert('gallery', $new);
    }

    public function update($art, $id) {
        $this->db->where('id', $id);
        $this->db->update('articles', $art);
    }

    public function getArticle($id) {
        $this->db->select('*')
                ->from('articles')
                ->where('id', $id);
        return $this->db->get()->row();
    }

    public function getArticlesByPlace($place = '', $limit = 5) {
        $this->db->select('*');
        $this->db->from('articles');
        $this->db->where('module', $place);
        $this->db->limit($limit);
        $this->db->order_by('publish_date', 'desc');
        return $this->db->get()->result();
    }

    public function getArticlesByCatID($catid = 0, $limit, $offset) {
        $this->db->select('*');
        $this->db->from('articles');
        $this->db->where('main_category', $catid);
        $this->db->limit($limit, $offset);
        $this->db->order_by('publish_date', 'desc');
        return $this->db->get()->result();
    }

    public function getArticlesCountByCatID($catid = 0) {
        $this->db->select('COUNT(1) AS total')
                ->from('articles')
                ->where('main_category', $catid);
        $q = $this->db->get()->row();
        return $q->total;
    }

    public function getArticlesCountForAdmin($catid = 0, $order, $status) {
        $this->db->select('COUNT(1) AS total')
                ->from('articles');
        if ($status != '')
            $this->db->where('status', $status);
        if ($order == 'date_asc') {
            $this->db->order_by('publish_date', 'asc');
        } else {
            $this->db->order_by('publish_date', 'desc');
        }
        $q = $this->db->get()->row();
        return $q->total;
    }

    public function getArticleBySeoUrl($url = '') {
        $this->db->select('*')
                ->from('articles')
                ->where('seo_url', $url);
        return $this->db->get()->row();
    }

    public function getImageOfArticle($id = 0) {
        $this->db->select('*')
                ->from('gallery')
                ->where('gallery_type', 'image')
                ->where('article_id', $id)
                ->order_by('main', 'desc')
                ->limit(1);
        return $this->db->get()->row();
    }

    public function getVideoOfArticle($id = 0) {
        $this->db->select('*')
                ->from('gallery')
                ->where('gallery_type', 'video')
                ->where('article_id', $id)
                ->order_by('main', 'desc')
                ->limit(1);
        return $this->db->get()->row();
    }

    public function getArticles($offset = 0, $limit = '0', $order = 'date_desc', $status = '', $catid = 0) {

        $this->db->select('*');
        $this->db->from('articles');
        if ($limit > 0)
            $this->db->limit($limit, $offset);
        if ($status != '')
            $this->db->where('status', $status);
        if ($catid > 0)
            $this->db->where('main_category', $cat);
        if ($order == 'date_asc') {
            $this->db->order_by('publish_date', 'asc');
        } else {
            $this->db->order_by('publish_date', 'desc');
        }
        return $this->db->get()->result();
    }

    public function getArticlesByCategories($id = 0, $offset = '', $limit = '100') {

        $this->db->select('*');
        $this->db->from('articles');

        if ($id > 0)
            $this->db->where('main_category', $id);
        if ($offset != '')
            $this->db->limit($limit, $offset);


        $this->db->order_by('publish_date', 'desc');
        return $this->db->get()->result();
    }

    public function checkArticleExists($url) {
        $this->db->select('COUNT(1) AS total')
                ->from('articles')
                ->where('seo_url', $url);
        $q = $this->db->get()->row();
        return $q->total;
    }

    public function getVideoByArticleID($article_id = 0) {
        $this->db->select('*');
        $this->db->from('gallery');
        $this->db->where('gallery_type', 'video');
        $this->db->where('article_id', $article_id);
        $this->db->order_by('id', 'asc');
        return $this->db->get()->row();
    }

    public function getGalleryByArticleID($article_id = 0) {
        $this->db->select('*');
        $this->db->from('gallery');
        $this->db->where('gallery_type', 'image');
        $this->db->where('article_id', $article_id);
        $this->db->order_by('id', 'asc');
        return $this->db->get()->result();
    }

    public function insert_rss($arr) {
        $this->db->insert('rss', $arr);
    }

    public function get_allrss() {
        $this->db->select('*');
        $this->db->from('rss');
        return $this->db->get()->result();
    }

    public function getRss($id = 0) {
        $this->db->select('*');
        $this->db->from('rss');
        $this->db->where('id', $id);
        return $this->db->get()->row();
    }

    public function update_rss($art, $id) {
        $this->db->where('id', $id);
        $this->db->update('rss', $art);
    }

    public function deleteArticle($article_id = 0) {
        $this->db->where('id', $article_id);
        $this->db->delete('articles');
    }

    public function deleteGalleryByArticleID($article_id = 0) {
        $this->db->where('article_id', $article_id);
        $this->db->delete('gallery');
    }

}