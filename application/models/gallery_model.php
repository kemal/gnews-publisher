<?php

class Gallery_model extends CI_Model {

	public function getPhotos($offset, $limit, $article_id =0) {
		$this->db->select('*');
		$this->db->limit($limit, $offset);
		$this->db->from('gallery');
		$this->db->where('gallery_type', 'image');
		if ($article_id>0)$this->db->where('article_id', $article_id);
		return $this->db->get()->result();
	}

	public function getVideos($offset, $limit, $article_id =0) {
		$this->db->select('*');
		$this->db->limit($limit, $offset);
		$this->db->from('gallery');
		$this->db->where('gallery_type', 'video');
		if ($article_id>0)$this->db->where('article_id', $article_id);
		return $this->db->get()->result();
	}

	public function getCountPhotos($article_id = 0) {
		$this->db->select('COUNT(1) AS total')
		->from('gallery');
		if ($article_id>0)$this->db->where('article_id', $article_id);
		$this->db->where('gallery_type', 'image');
		$q = $this->db->get()->row();

		return $q->total;
	}

	public function getCountVideos($article_id = 0) {
		$this->db->select('COUNT(1) AS total')
		->from('gallery');
		$this->db->where('gallery_type', 'video');
		if ($article_id>0)$this->db->where('article_id', $article_id);
		$q = $this->db->get()->row();

		return $q->total;
	}

	public function getPhoto($id) {
		$this->db->select('*');
		$this->db->where('id', $id);
		$this->db->from('gallery');
		return $this->db->get()->row();
	}

	public function getVideo($article_id) {
		$this->db->select('*');
		$this->db->where('article_id', $article_id);
		$this->db->where('gallery_type', 'video');
		$this->db->from('gallery');
		return $this->db->get()->row();
	}
	public function updateVideo($v,$id=0){
		$video = $this->getVideo($id);
		if ($video){
			$this->db->where('article_id', $id);
			$this->db->update('gallery', $v);
		}else{
			$this->db->insert('gallery', $v);
		}
		return;
	}

	public function updatePhoto($id,$art) {
		$this->db->where('id', $id);
		$this->db->update('gallery', $art);
	}

	public function addPhoto($art) {
		$this->db->insert('gallery', $art);
		return $this->db->insert_id();
	}

	public function getDelete($id){
		$this->db->where('id', $id);
		$this->db->delete('gallery');
	}

	public function getImageGallery($article_id = 0, $page=0){
		$images = $this->getPhotos($page, 1, $article_id);
		return $images[0];
	}

}