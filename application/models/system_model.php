<?php

class System_model extends CI_Model {

    public function getSystemSetting() {
        $this->db->select('*')
                ->from('system');
        return $this->db->get()->result();
    }

}