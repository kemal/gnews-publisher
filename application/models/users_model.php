<?php

class Users_model extends CI_Model {

    public function getUsers($offset = '', $limit = '100') {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->limit($limit, $offset);
        return $this->db->get()->result();
    }

    public function getCountUsers() {
        $this->db->select('COUNT(1) AS total')
                ->from('users');
        $q = $this->db->get()->row();
        return $q->total;
    }

    public function getUser($id) {
        $this->db->select('*')
                ->from('users');
        $this->db->where('id', $id);
        return $this->db->get()->row();
    }

    private function checkLogin($email='', $password='') {
        $this->db->select('COUNT(1) AS total')
                ->from('users');
        $this->db->where('email', $email);
        $this->db->where('password', $password);
        $q = $this->db->get()->row();
        return $q->total;
    }
    public function checkLoginByID($id=0) {
        $this->db->select('COUNT(1) AS total')
                ->from('users');
        $this->db->where('id', $id);
        $q = $this->db->get()->row();
        return $q->total;
    }

    public function getUserByLogin($email, $password) {

        $total = $this->checkLogin($email, $password);
        if ($total) {
            $this->db->select('*')
                    ->from('users');
            $this->db->where('email', $email);
            $this->db->where('password', $password);
            return $this->db->get()->row();
        } else {
            return;
        }
    }

    public function insert($u) {
        $this->db->insert("users", $u);
    }

    public function update($u, $id) {
        $this->db->where('id', $id);
        $this->db->update("users", $u);
    }

    public function nameExist($name, $id = 0) {
        $this->db->select('COUNT(1) AS total')
                ->from('users')
                ->where('name', $name);
        if ($id > 0)
            $this->db->where('id != ' . $id);

        $q = $this->db->get()->row();
        return $q->total;
    }

    public function emailExist($email, $id = 0) {
        $this->db->select('COUNT(1) AS total')
                ->from('users')
                ->where('email', $email);
        if ($id > 0)
            $this->db->where('id != ' . $id);
        $q = $this->db->get()->row();
        return $q->total;
    }

}