<?php

class Categories_model extends CI_Model {

    public function getCategories() {
        $this->db->select('* ')
                ->from('categories')
                ->order_by('name', 'asc');
        return $this->db->get()->result();
    }
    public function getCategory($id=0) {
        $this->db->select('* ')
                ->from('categories')
                ->where('id', $id);
        return $this->db->get()->row();
    }    
    
 
    public function getCategoryBySeoUrl($url='') {
        $this->db->select('*')
                ->from('categories')
                ->where('seo_url', $url);
        return $this->db->get()->row();
    }        
    
    public function update($u, $id) {
        $this->db->where('id', $id);
        $this->db->update('categories', $u);
    }
    public function save($u) {
        $this->db->insert('categories', $u);
    }
    public function getEntries($limit, $offset) {
        $this->db->select('e.*, m.media_value, media_type, u.name ')
                ->from('entries e')
                ->join('users u', 'u.id = e.owner', 'left')
                ->join('media m', 'm.id = e.media', 'left')
                ->order_by('e.created_at', 'desc')
                ->limit($limit, $offset);
        return $this->db->get()->result();
    }

    public function getTotalEntriesCount() {
        $this->db->select('COUNT(1) AS toplam')
                ->from('entries')
                ->limit(1);
        $q = $this->db->get()->row();
        return $q->toplam;
    }

    public function getMembers($limit, $offset) {
        $this->db->select('*')
                ->from('users')
                ->order_by('created_at', 'desc')
                ->limit($limit, $offset);
        return $this->db->get()->result();
    }

    public function getTotalMemberCount() {
        $this->db->select('COUNT(1) AS toplam')
                ->from('users')
                ->limit(1);
        $q = $this->db->get()->row();
        return $q->toplam;
    }

    public function getComments($limit, $offset) {
        $this->db->select('c.*, e.title, u.name ')
                ->from('comments c')
                ->join('users u', 'u.id = c.user', 'left')
                ->join('entries e', 'e.id = c.entry', 'left')
                ->order_by('c.created_at', 'desc')
                ->limit($limit, $offset);
        return $this->db->get()->result();
    }

    function updateCommentStatus($id, $status) {
        $data = Array('durum' => $status);
        $this->db->where('id', $id);
        $this->db->update('comments', $data);
    }

    function updateUserStatus($id, $status) {
        $data = Array('durum' => $status);
        $this->db->where('id', $id);
        $this->db->update('users', $data);
    }

    function updateListelemeStatus($id, $status) {
        $data = Array('featured' => $status);
        $this->db->where('id', $id);
        $this->db->update('entries', $data);
    }

    function updateEntryStatus($id, $status) {
        $data = Array('status' => $status);
        $this->db->where('id', $id);
        $this->db->update('entries', $data);
    }

    public function getTotalCommentCount() {
        $this->db->select('COUNT(1) AS toplam')
                ->from('comments')
                ->limit(1);
        $q = $this->db->get()->row();
        return $q->toplam;
    }

    public function getLastPhotos($limit, $offset) {
        $this->db->select('*')
                ->from('photo')
                ->order_by('created_at', 'desc')
                ->limit($limit, $offset);
        return $this->db->get()->result();
    }

    public function getTotalPhotoCount() {
        $this->db->select('COUNT(1) AS toplam')
                ->from('photo')
                ->limit(1);
        $q = $this->db->get()->row();
        return $q->toplam;
    }

    public function changeTweetStatus($tweetId, $newStatus) {
        $this->db->where('tweet_id', $tweetId);
        $this->db->update('twitter', array(
            'status' => $newStatus
        ));
    }

    public function changePhotoStatusByTweetId($tweetId, $newStatus) {
        $this->db->where('item_id', $tweetId);
        $this->db->where('item_type', 1); // twitter
        $this->db->update('photo', array(
            'status' => $newStatus
        ));
    }

    public function changePhotoStatusById($photoId, $newStatus) {
        $this->db->where('id', $photoId);
        $this->db->update('photo', array(
            'status' => $newStatus
        ));
    }

    public function getAllUsers($type = 'site') {
        $this->db->select('*')
                ->from('users')
                ->where('member_type', 'site')
                ->where('durum', 'Active')
                ->order_by('created_at', 'desc');
        return $this->db->get()->result();
    }

    public function getAllEntries() {
        $this->db->select('*')
                ->from('entries')
                ->order_by('created_at', 'desc');
        return $this->db->get()->result();
    }

    public function saveMedia($photoCopy) {
        $media = Array('media_type' => 'image', 'media_value' => '06/' . $photoCopy);

        $this->db->insert('media', $media);
        $id = $this->db->insert_id();
        return $id;
    }

    public function saveEntry($u) {
        $this->db->insert('entries', $u);
    }

    public function saveComment($u) {
        $this->db->insert('comments', $u);
    }

}