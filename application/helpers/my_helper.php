<?php

function my_get($q) {
    if (isset($_GET[$q])) {
        return trim(strip_tags($_GET[$q]));
    }
    $url = parse_url($_SERVER["REQUEST_URI"]);
    if (isset($url['query'])) {
        parse_str($url['query'], $arr);
        if (isset($arr[$q]) && $arr[$q] != '') {
            if (is_array($arr[$q])) {
                return $arr[$q];
            }
            return trim(strip_tags($arr[$q]));
        }
    }
    return false;
}
function get_fb_picture($id, $type = 'square') {
    return 'http://graph.facebook.com/' . $id . '/picture?type=' . $type;
}
function friendly_url($url) {
    $url = trim($url);
    $find = array('<b>', '</b>');
    $url = str_replace($find, '', $url);
    $url = preg_replace('/<(\/{0,1})img(.*?)(\/{0,1})\>/', 'image', $url);
    $find = array(' ', '&quot;', '&amp;', '&', '\r\n', '\n', '/', '\\', '+', '<', '>');
    $url = str_replace($find, '-', $url);
    $find = array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ë', 'Ê');
    $url = str_replace($find, 'e', $url);
    $find = array('í', 'ı', 'ì', 'î', 'ï', 'I', 'İ', 'Í', 'Ì', 'Î', 'Ï');
    $url = str_replace($find, 'i', $url);
    $find = array('ó', 'ö', 'Ö', 'ò', 'ô', 'Ó', 'Ò', 'Ô');
    $url = str_replace($find, 'o', $url);
    $find = array('á', 'ä', 'â', 'à', 'â', 'Ä', 'Â', 'Á', 'À', 'Â');
    $url = str_replace($find, 'a', $url);
    $find = array('ú', 'ü', 'Ü', 'ù', 'û', 'Ú', 'Ù', 'Û');
    $url = str_replace($find, 'u', $url);
    $find = array('ç', 'Ç');
    $url = str_replace($find, 'c', $url);
    $find = array('ş', 'Ş');
    $url = str_replace($find, 's', $url);
    $find = array('ğ', 'Ğ');
    $url = str_replace($find, 'g', $url);
    $find = array('/[^A-Za-z0-9\-<>]/', '/[\-]+/', '/<[^>]*>/');
    $repl = array('', '-', '');
    $url = preg_replace($find, $repl, $url);
    $url = str_replace('--', '-', $url);
    $url = strtolower($url);
    return $url;
}

if (!function_exists('is_ajax')) {

    function is_ajax() {
        return (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');
    }

}

 

function rakam($r) {
    if ($r < 0)
        return 0;
    return abs(intval($r));
}
 
function rnd_str($length = 6) {

    $all = '123456789ABCDEFGHJKLMNPRSTVYZXW';
    $result = '';

    for ($i = 0; $i < $length; $i++) {
        $result .= $all[mt_rand(0, strlen($all) - 1)];
    }

    return $result;
}

function get_cf_ip() {
    if (isset($_SERVER["HTTP_CF_CONNECTING_IP"])) {
        $_SERVER["REMOTE_ADDR"] = $_SERVER["HTTP_CF_CONNECTING_IP"];
    }
    return $_SERVER["REMOTE_ADDR"];
}

function get_mdate($fileName, $filePath) {
    return filemtime($filePath . $fileName);
}

function today() {
    $datestring = "%Y-%m-%d %h:%i";
    $time = time();
    return mdate($datestring, $time);
}
function dirpath() {
    $datestring = "%Y-%m-%d";
    $time = time();
    return mdate($datestring, $time);
}


function getImagePath(){
    $dirname = dirpath();
    $filename = ("../public/images/content/" .$dirname . "/");
  
    if (file_exists($filename)) {
       return $filename;
    } else {
        mkdir("../public/images/content/" .$dirname, 0777);
        return $filename;
    }
}