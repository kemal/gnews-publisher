<?php

class Ajax extends MY_Controller {

    function __construct() {

        parent::__construct();
        $this->load->model('categories_model');
        $this->load->model('articles_model');
        // is a ajax request?
        if (!is_ajax())
            show_404();

        // no cache
        $this->output->set_header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate");
        $this->output->set_header("Cache-Control: post-check=0, pre-check=0", false);
        $this->output->set_header("Pragma: no-cache");
        // json header
        $this->output->set_content_type('application/json');
    }

    function deletearticle() {


        $id = $this->input->post("id");
        $this->articles_model->deleteArticle($id);
        $this->articles_model->deleteGalleryByArticleID($id);
        $this->output->set_output(json_encode(array(
                    'return' => true
                )));
    }

}