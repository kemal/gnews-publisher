<?php

class Article extends MY_Controller {

    const ITEM_PER_PAGE = 50;

    function __construct() {
        parent::__construct();
        $this->load->model('categories_model');
        $this->load->model('articles_model');
        $this->article = new Articles();
        $this->auth = new Auth();
        $this->auth->check_access();
        $this->auth->isAllowed('Articles');
    }

    public function index() {
        $page = $this->getCurrentOffset();
        $order = 'date_desc';
        $list = '';
        if ($this->input->get('order')) $order = $this->input->get('order');
        if ($this->input->get('list')) $list = $this->input->get('list');
        
        $total = $this->articles_model->getArticlesCountForAdmin(0, $order, $list);
        $this->data['articles'] = $this->articles_model->getArticles($page, self::ITEM_PER_PAGE, $order, $list);
        $config = array();
        $config['base_url'] = site_url() . 'admin/articles';
        $config['total_rows'] = $total;
        $config['per_page'] = self::ITEM_PER_PAGE;
        $config['page_query_string'] = TRUE;
        $config['num_links'] = 3;
        $config['uri_segment'] = 3;
        $this->myPaginationInit($config);
        $this->pagination->initialize($config);
        $this->data['pagination_links'] = $this->pagination->create_links();
        $this->twig->display('panel/articles.html', $this->data);
    }

    public function newarticle() {
        $this->data['categories'] = $this->categories_model->getCategories();
        $this->data['article'] = $this->articles_model->openLastArticle();
        $this->data['modules'] = $this->articles_model->getModules();
        $this->twig->display('panel/edit.html', $this->data);
    }

    public function edit() {
        $action = trim($this->uri->segment(4));
        if ($action === 'update') {
            $this->update();
        } else {
            $this->data['categories'] = $this->categories_model->getCategories();
            $this->data['article'] = $this->articles_model->getArticle(rakam($action));
            $this->data['modules'] = $this->articles_model->getModules();
            $this->data['gallery'] = $this->articles_model->getGalleryByArticleID($action);
            $this->data['video'] = $this->articles_model->getVideoByArticleID($action);
            $this->twig->display('panel/edit.html', $this->data);
        }
    }

    public function view() {
        $id = rakam($this->uri->segment(4));
        $this->data['categories'] = $this->categories_model->getCategories();
        $this->data['modules'] = $this->articles_model->getModules();
        $this->data['article'] = $this->articles_model->getArticle($id);
        $this->data['gallery'] = $this->articles_model->getGalleryByArticleID($id);
        $this->data['video'] = $this->articles_model->getVideoByArticleID($id);
        $this->twig->display('panel/edit.html', $this->data);
    }

    private function update() {
        $art = new stdClass;
        $id = $this->input->post("id");
        $art->title = $this->input->post("title");
        $art->summary = $this->input->post("summary");
        $art->story = $this->input->post("story");
        $art->status = $this->input->post("status");
        $art->featured = 0;
        $art->main_category = $this->input->post("main_category");
        $art->module = $this->input->post("module");
        $art->author_id = 0;
        $art->publish_date = $this->input->post("publish_date") . ' ' . $this->input->post("publish_hour");
        $art->modified_date = today();
        $art->archived = 0;
        $art->seo_url = friendly_url($this->input->post("title"));
        $art->seo_title = $this->input->post("seo_title");
        $art->seo_desc = $this->input->post("seo_desc");
        $art->seo_keywords = $this->input->post("seo_keywords");
        $this->articles_model->update($art, $id);

        $this->data['categories'] = $this->categories_model->getCategories();
        $this->data['modules'] = $this->articles_model->getModules();
        $this->data['gallery'] = $this->articles_model->getGalleryByArticleID($id);
        $this->data['video'] = $this->articles_model->getVideoByArticleID($id);
        $this->data['article'] = $this->articles_model->getArticle($id);
        $this->twig->display('panel/edit.html', $this->data);
    }

    protected function myPaginationInit(&$config) {
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0)">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li class="next">';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['first_link'] = 'İlk Sayfa';
        $config['last_link'] = 'Son Sayfa';
        $config['prev_link'] = 'Önceki';
        $config['next_link'] = 'Sonraki';
    }

    protected function getCurrentOffset() {
        $page = rakam($this->uri->segment(3));
        return !$page ? 0 : $page;
    }
 
}