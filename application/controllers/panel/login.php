<?php
class Login extends MY_Controller {
    const ITEM_PER_PAGE = 50;
    function __construct() {
        parent::__construct();
        $this->load->model('users_model');
    }

    public function index() {
        if ($this->form_validation->run('admin/login') == true) {
            $email = $this->input->post('email');
            $password = $this->input->post('password');
            $rst = $this->users_model->getUserByLogin($email, $password);
            if ($rst) {
                $userdata = array(
                    'id' => $rst->id,
                    'name' => $rst->name,
                    'email' => $rst->email,
                    'level' => $rst->level,
                    'seo_url' => $rst->seo_url,
                    'status' => $rst->status,
                    'logged_in' => TRUE
                );

                $this->session->set_userdata($userdata);
		  redirect('admin', 'refresh');
            } else {
                $this->data['error'] = true;
            }
        }

        $this->twig->display('panel/login.html', $this->data);
    }

    public function logout(){
    	 $this->session->unset_userdata('level');
    	 redirect('admin/login', 'refresh');
    }    
    
    public function failed(){
    	
    	$this->twig->display('panel/failed.html', $this->data);
    }
    
}