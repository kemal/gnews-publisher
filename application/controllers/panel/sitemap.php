<?php

class Sitemap extends MY_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('categories_model');
		$this->load->model('articles_model');
		$this->article = new Articles();
	}

	public function index() {
		header("Content-Type: text/xml");
		echo '<urlset xmlns="http://www.google.com/schemas/sitemap/0.84">' ."\n";
		
		$articles = $this->articles_model->getArticles(0, 50000, '', 'Live');
		foreach ($articles as $a){
			echo "<url>\n";
			echo "<loc>" . base_url($a->seo_url) . "</loc>\n";
			echo "<lastmod>". $a->modified_date ."</lastmod>\n";
			echo "<changefreq>weekly</changefreq>\n";
			echo "<priority>". getPriority($a->modified_date) ."</priority>\n";
			echo "</url>\n";
		}
		$articles = $this->categories_model->getCategories();
		foreach ($articles as $a){
			echo "<url>\n";
			echo "<loc>" . base_url($a->seo_url) . "</loc>\n";
			echo "<lastmod>". today() ."</lastmod>\n";
			echo "<changefreq>daily</changefreq>\n";
			echo "<priority>1.0</priority>\n";
			echo "</url>\n";
		}
		echo "</urlset>\n";
	}

}

