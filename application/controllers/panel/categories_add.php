<?php

class Categories extends MY_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('categories_model');
		$this->auth = new Auth();
		$this->auth->check_access();
		$this->auth->isAllowed('Categories');

	}

	public function index() {
		$act = $this->uri->segment(3);
		switch ($act){

			case 'new':
				$this->newcat();
				break;
			case 'add_category':
				$this->add_category();
				break;
			default :
				echo "do nthing";
		}
	}

	public function newcat() {
		$this->twig->display('panel/category.html', $this->data);

	}

	public function add_category() {


		if ($this->form_validation->run('categories') == false) {
			$this->twig->display('panel/category.html', $this->data);
		} else {
			$art = new stdClass;
			$art->name = $this->input->post("name");
			$art->seo_url = friendly_url($this->input->post("name"));
			$art->seo_title = $this->input->post("seo_title");
			$art->seo_desc = $this->input->post("seo_desc");
			$art->seo_keywords = $this->input->post("seo_keywords");
			$this->categories_model->save($art, $id);
		}
	}


}