<?php

class Categories extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('categories_model');
        $this->load->model('articles_model');
        $this->auth = new Auth();
        $this->auth->check_access();
        $this->auth->isAllowed('Categories');

    }

    public function index() {
        $act = $this->uri->segment(3);
        switch ($act) {

            case 'new':
                $this->newcat();
                break;
            case 'add_category':
                $this->add_category();
                break;

            case 'edit_category':
                $this->edit_category();
                break;

            case 'edit':
                $this->edit();
                break;

            case 'list':
                $this->articleList();
                break;

            default :
                $this->all();
        }
    }

    public function edit() {
        $this->data['action'] = '/admin/category/edit_category';
        $id = rakam($this->uri->segment(4));
        $this->data['category'] = $this->categories_model->getCategory($id);
        $this->twig->display('panel/update_category.html', $this->data);
    }

    public function articleList() {
        $id = rakam($this->uri->segment(4));
        $this->data['articles'] = $this->articles_model->getArticlesByCategories($id);
        $this->twig->display('panel/articles.html', $this->data);
    }

    public function edit_category() {
        $art = new stdClass;
        $id = $this->input->post("id");
        $art->name = $this->input->post("name");
        $art->seo_url = friendly_url($this->input->post("name"));
        $art->seo_title = $this->input->post("seo_title");
        $art->seo_desc = $this->input->post("seo_desc");
        $art->seo_keywords = $this->input->post("seo_keywords");
        $art->order_id = rakam($this->input->post('order_id'));
        $this->categories_model->update($art, $id);
        $this->data['alert'] = '<div class="alert alert-success">Category has been update! </div>';
        $this->data['category'] = $this->categories_model->getCategory($id);
        $this->twig->display('panel/update_category.html', $this->data);
    }

    public function all() {

        $this->data['categories'] = $this->categories_model->getCategories();
        $this->twig->display('panel/categories.html', $this->data);
    }

    public function newcat() {
        $this->data['action'] = '/admin/category/add_category';
        $this->twig->display('panel/update_category.html', $this->data);
    }

    public function add_category() {
        $art = new stdClass;
        $art->name = $this->input->post("name");
        $art->seo_url = friendly_url($this->input->post("name"));
        $art->seo_title = $this->input->post("seo_title");
        $art->seo_desc = $this->input->post("seo_desc");
        $art->seo_keywords = $this->input->post("seo_keywords");
        $this->categories_model->save($art);
        $id = $this->db->insert_id();
        $this->data['alert'] = '<div class="alert alert-success">Category has been inserted! </div>';
        $this->data['action'] = '/admin/category/edit_category';
        $this->data['category'] = $this->categories_model->getCategory($id);
        $this->twig->display('panel/update_category.html', $this->data);
    }

}