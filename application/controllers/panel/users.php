<?php

class Users extends MY_Controller {

    const ITEM_PER_PAGE = 100;

    private $error = Array();

    function __construct() {
        parent::__construct();
        $this->load->model('categories_model');
        $this->load->model('articles_model');
        $this->load->model('users_model');
        $this->article = new Articles();
		$this->auth = new Auth();
        $this->auth->check_access();
        $this->auth->isAllowed('User');

    }

    public function index() {
        $page = $this->getCurrentOffset();
        $total = $this->users_model->getCountUsers(0);
        $this->data['users'] = $this->users_model->getUsers($page, self::ITEM_PER_PAGE);
        $config = array();
        $config['base_url'] = site_url() . 'admin/users';
        $config['total_rows'] = $total;
        $config['per_page'] = self::ITEM_PER_PAGE;
        $config['num_links'] = 3;
        $config['uri_segment'] = 3;
        $this->myPaginationInit($config);
        $this->pagination->initialize($config);
        $this->data['pagination_links'] = $this->pagination->create_links();
        $this->twig->display('panel/users.html', $this->data);
    }

    public function update() {
        if ($this->form_validation->run('admin/user/add') == false) {
            if (rakam($this->input->post('id')) > 0) {
                    $this->edit($this->input->post('id'));
            }else{
            $this->add();
            }
        } else {
            $u = new stdClass();
            $u->name = $this->input->post('name');
            $u->email = $this->input->post('email');
            $u->level = $this->input->post('level');
            $u->status = $this->input->post('status');
            $u->password = $this->input->post('password');
            $u->seo_url = friendly_url($this->input->post('name'));
            $id = rakam($this->input->post('id'));
            if ($id == 0) {
                if ($this->users_model->emailExist($u->email) > 0) {
                    $this->error[] = 'Email address is using by another user';
                }
                if ($this->users_model->nameExist($u->name) > 0) {
                    $this->error[] = 'Name is using by another user';
                }
                if (count($this->error) == 0) {
                    $this->users_model->insert($u);
                    $this->index();
                } else {
                    $this->add();
                }
            } else {
                if ($this->users_model->emailExist($u->email, $id) > 0) {
                    $this->error[] = 'Email address is using by another user';
                }
                if ($this->users_model->nameExist($u->name, $id) > 0) {
                    $this->error[] = 'Name is using by another user';
                }
                if (count($this->error) == 0) {
                    $this->users_model->update($u, $id);
                    $this->index();
                } else {
                    $this->edit($id);
                }
            }
        }
    }

    protected function myPaginationInit(&$config) {
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0)">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li class="next">';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['first_link'] = 'İlk Sayfa';
        $config['last_link'] = 'Son Sayfa';
        $config['prev_link'] = 'Önceki';
        $config['next_link'] = 'Sonraki';
    }

    protected function getCurrentOffset() {
        $page = rakam($this->uri->segment(3));
        return !$page ? 0 : $page;
    }

    public function add() {
        $this->data['user'] = Array('id' => 0);
        $this->data['error'] = $this->error;
        $this->twig->display('panel/update_user.html', $this->data);
    }

    public function edit($id = 0) {
        if ($id == 0)
            $id = rakam($this->uri->segment(4));
        $this->data['error'] = $this->error;

        $this->data['user'] = $this->users_model->getUser($id);
        $this->twig->display('panel/update_user.html', $this->data);
    }

}