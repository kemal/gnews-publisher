<?php

class Rss_reader extends MY_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('categories_model');
		$this->load->model('articles_model');
		$this->auth = new Auth();
		$this->auth->check_access();
		$this->auth->isAllowed('RSS');
	}
	public function index() {
		$this->data['rsslist'] = $this->articles_model->get_allrss();
		$this->twig->display('panel/rss_list.html', $this->data);
	}
        
        public function update(){
            
        }

        public function download() {
		
		$id = $this->uri->segment(4);
		$rss=$this->articles_model->getRss($id);
		$rss_url = $rss->source;
		$catid = $rss->cat_id;
                
		$this->load->library('RSSParser', array('url' => $rss_url, 'life' => 2));
		$data = $this->rssparser->getFeed(10);
		$article_data = Array();
		foreach ($data as $item) {
			$desc = $item['description'];
			$desc = preg_replace("/<img[^>]+\>/i", " ", $desc);
			$desc = strip_tags($desc);

			$article = Array();
                        
			$article['title'] = $item['title'];
			$article['desc'] = $desc;
			$article['pubDate'] = $item['pubDate'];

			$images = $item['description'];
			$gallery = Array();
			$doc = new DOMDocument();
			$images = false;
                        $images = str_replace('&', '&amp;', $images);
			if($images){
                        $doc->loadHTML($images);
			$tags = $doc->getElementsByTagName('img');
			foreach ($tags as $tag) {
				$gallery[] = $tag->getAttribute('src');
			}
                        }
			$id = $this->insert($article, $gallery, $catid);
                        if ($id){
                        $article['id']=$id;
                        $article_data[]=$article;
                        }
		}
		$this->data['articles']=$article_data;
		$this->twig->display('panel/rss_import_preview.html', $this->data);
	}

	private function insert($article, $gallery,$catid) {

		$url = friendly_url($article['title']);
		$total = $this->articles_model->checkArticleExists($url);

		if ($total == 0) {
			$art = new stdClass;
			$art->title = $article['title'];
			$art->summary = trim($article['desc']);
			$art->story = trim($article['desc']);
			$art->status = 'Pending';
			$art->featured = 0;
			$art->main_category = $catid;
			$art->author_id = 0;
			$art->publish_date = today();
			$art->modified_date = today();
			$art->archived = 0;
			$art->seo_url = friendly_url($article['title']);
			$art->seo_title = $article['title'];
			$art->seo_desc = '';
			$art->seo_keywords = '';
			$id = $this->articles_model->insertnew($art);
			foreach ($gallery as $g => $value) {
				$filename = basename($value);
				$img = file_get_contents($value);
				file_put_contents(getImagePath() . $filename, $img);
				$this->articles_model->addImage2Gallery($filename, $id, dirpath());
			}
                        return $id;
		}else{
                    return 0;
                }
	}

	public function insertrss() {
		$this->data['categories'] = $this->categories_model->getCategories();
		if ($this->form_validation->run('admin/rss/insert') == true) {
			$rss = new stdClass();
			$rss->name = $this->input->post('name');
			$rss->source = $this->input->post('source');
			$rss->cat_id = $this->input->post('cat_id');
			$this->articles_model->insert_rss($rss);
			$this->data['error']= "New source inserted!";
		}
		$this->twig->display('panel/rss_insert.html', $this->data);
	}
	public function editrss() {
		$id = rakam($this->uri->segment(4));
		$this->data['categories'] = $this->categories_model->getCategories();
		
		if ($this->form_validation->run('admin/rss/edit') == true) {
			$rss = new stdClass();
			$rss->name = $this->input->post('name');
			$rss->source = $this->input->post('source');
			$rss->cat_id = $this->input->post('cat_id');
			$this->articles_model->update_rss($rss, $id);
			$this->data['error']= "Source Updated!";
		}
		
		$this->data['rss']=$this->articles_model->getRss($id);
		$this->twig->display('panel/rss_edit.html', $this->data);
	}

}

