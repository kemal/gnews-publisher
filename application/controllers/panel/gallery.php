<?php

class Gallery extends MY_Controller {

    const ITEM_PER_PAGE = 10;

    function __construct() {
        parent::__construct();
        $this->load->model('gallery_model');
        $this->load->model('articles_model');
        $this->auth = new Auth();
        $this->auth->check_access();
        $this->auth->isAllowed('Gallery');
    }

    public function index() {
        $action = trim($this->uri->segment(3));
        switch ($action) {
            case 'photos':
                $this->photos();
                break;
                case 'videos':
                	$this->videos();
                	break;
                	case 'video':
                		$this->video();
                		break;                
            case 'edit':
                $this->edit();
                break;
            case 'add':
                $this->add();
                break;

            default :
                echo "gallery";
        }
    }

    public function add() {
        $id = '';
        $id = rakam($this->uri->segment(4));
        $this->data['photo'] = Array('article_id' => $id, 'id' => 0);
        $this->twig->display('panel/photo.html', $this->data);
    }

    public function edit() {
        $id = rakam($this->uri->segment(4));
        $this->data['photo'] = $this->gallery_model->getPhoto($id);
        $this->twig->display('panel/photo.html', $this->data);
    }
    
    
    public function video() {
    	$id = rakam($this->uri->segment(4));
    	$this->data['id']=$id;
    	$this->data['video'] = $this->gallery_model->getVideo($id);
    	
    	if ($this->form_validation->run('admin/gallery/video') == true) {
    		$v = new stdClass;
    		$v->article_id = $this->input->post('article_id');
    		$v->title = $this->input->post('title');
    		$v->alt = $this->input->post('alt');
    		$v->gallery_type = 'video';
    		$v->filename = $this->input->post('filename');
    		$this->gallery_model->updateVideo($v, $id);
		}
		$this->data['video'] = $this->gallery_model->getVideo($id);
    	$this->twig->display('panel/video.html', $this->data);
    }
    

    public function delete() {
        $id = '';
        $id = rakam($this->uri->segment(4));
        $p = $this->gallery_model->getPhoto($id);
        if ($p) {
            @unlink("images/content/" . $p->dirpath . '/' . $p->filename);
            $this->gallery_model->getDelete($id);
        }
        echo "Photo has been deleted!";
    }
    
    public function vdelete() {
    	$id = '';
    	$id = rakam($this->uri->segment(4));
    	$this->gallery_model->getDelete($id);
    	echo "Video has been deleted!";
    }

    public function update() {
        $id = $this->input->post("id");
        $article_id = $this->input->post("article_id");
        getImagePath();
        $config['upload_path'] = 'images/content/' . dirpath() . '/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = '100';
        $config['max_width'] = '1024';
        $config['max_height'] = '668';
        $art = new stdClass;
        $this->load->library('upload', $config);
        try {
            if ($this->upload->do_upload('filename')) {
                $data = $this->upload->data();
                $art->filename = $data['file_name']; //$this->input->post('main');
                $art->dirpath = dirpath();
            }else{
                Logger::error($this->upload->display_errors());
            }
        } catch (Exception $e) {
            Logger::error($e);
        }
        $art->alt = $this->input->post('alt');
        $art->title = $this->input->post('title');
        $art->main = $this->input->post('main');
        $art->gallery_type = 'image';

        $art->article_id = $this->input->post('article_id');
        if ($id > 0) {
            $this->gallery_model->updatePhoto($id, $art);
        } elseif ($id == 0 || $article_id > 0) {
            $id = $this->gallery_model->addPhoto($art);
        } else {
            $this->photos();
        }
        
        $this->data['photo'] = $this->gallery_model->getPhoto($id);
        $this->twig->display('panel/photo.html', $this->data);
    }

    public function photos() {
        $page = $this->getCurrentOffset();
        $article_id = 0;
        if ($this->input->get('article_id')) $article_id = $this->input->get('article_id');
        $total = $this->gallery_model->getCountPhotos($article_id);
        
        $this->data['gallery'] = $this->gallery_model->getPhotos($page, self::ITEM_PER_PAGE, $article_id);
        $config = array();
        $config['base_url'] = site_url() . 'admin/gallery/photos';
        $config['total_rows'] = $total;
        $config['per_page'] = self::ITEM_PER_PAGE;
        $config['num_links'] = 3;
        $config['uri_segment'] = 4;
        $this->myPaginationInit($config);
        $this->pagination->initialize($config);
        $this->data['pagination_links'] = $this->pagination->create_links();
        $this->twig->display('panel/gallery.html', $this->data);
    }

    protected function myPaginationInit(&$config) {
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0)">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li class="next">';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['first_link'] = 'İlk Sayfa';
        $config['last_link'] = 'Son Sayfa';
        $config['prev_link'] = 'Önceki';
        $config['next_link'] = 'Sonraki';
    }

    protected function getCurrentOffset() {
        $page = rakam($this->uri->segment(4));
        return !$page ? 0 : $page;
    }
    
    
    public function videos() {
    	$page = $this->getCurrentOffset();
    	$article_id = 0;
    	if ($this->input->get('article_id')) $article_id = $this->input->get('article_id');
    	$total = $this->gallery_model->getCountVideos($article_id);
    	$this->data['gallery'] = $this->gallery_model->getVideos($page, self::ITEM_PER_PAGE, $article_id);
    	$config = array();
    	$config['base_url'] = site_url() . 'admin/gallery/videos';
    	$config['total_rows'] = $total;
    	$config['per_page'] = self::ITEM_PER_PAGE;
    	$config['num_links'] = 3;
    	$config['uri_segment'] = 4;
    	$this->myPaginationInit($config);
    	$this->pagination->initialize($config);
    	$this->data['pagination_links'] = $this->pagination->create_links();
    	$this->twig->display('panel/videos.html', $this->data);
    }

}