<?php

class Home extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->auth = new Auth();
        $this->auth->check_access();
    }

    public function index() {
        $this->twig->display('panel/index.html', $this->data);
    }

}