<?php

class Home extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('categories_model');
        $this->article = new Articles();
        $this->system_settings = new Appsystem();
        }

    public function index() {
        $this->article->limit = 3;
        $this->data['categories'] = $this->categories_model->getCategories();
        $this->data['articles'] = $this->article->getFrontArticles();
        $this->data['featured'] = $this->article->getArticlesByPlace('featured',4);
        $this->data['home'] = $this->article->getArticlesByPlace('home',3);
        $this->data['colm'] = $this->article->getArticlesByPlace('colm',4);
        $this->data['flash'] = $this->article->getArticlesByPlace('flash');
        $this->data['system'] = $this->system_settings->getSystemSetting();
        
        $this->twig->display('default/index.html', $this->data);
    }

}
