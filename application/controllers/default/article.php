<?php

class Article extends MY_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('categories_model');
		$this->load->model('articles_model');
		$this->load->model('gallery_model');
		$this->article = new Articles();
	}

	public function index() {
		$url = trim($this->uri->segment(2));
		$this->data['categories'] = $this->categories_model->getCategories();
		$this->data['latest'] = $this->article->getFrontArticles();
		$this->data['article'] = $this->articles_model->getArticleBySeoUrl($url);
		$this->data['image'] = $this->articles_model->getImageOfArticle($this->data['article']->id);
		$this->data['video'] = $this->articles_model->getVideoOfArticle($this->data['article']->id);
		$this->data['gallery'] = $this->gallery_model->getCountPhotos($this->data['article']->id);
		$this->twig->display('default/article.html', $this->data);
	}

	public function gallery() {
		$url = trim($this->uri->segment(2));
		$this->data['categories'] = $this->categories_model->getCategories();
		$this->data['latest'] = $this->article->getFrontArticles();
		$this->data['article'] = $this->articles_model->getArticleBySeoUrl($url);
		$p = (int)$this->input->get('p');
		
		$count = $this->gallery_model->getCountPhotos($this->data['article']->id);
		$count = $count-1;
		if ($count > $p) {
			$this->data['link']=base_url() .'gallery/'. $this->data['article']->seo_url . '?p='. ($p +1);
		}
		if ($count == $p){
			$this->data['link']=base_url() .'article/'. $this->data['article']->seo_url;
		}
		$this->data['image'] = $this->gallery_model->getImageGallery($this->data['article']->id, $p);
		$this->data['page_count'] = ($count + 1) . '/'. ($p + 1);
		$this->twig->display('default/gallery.html', $this->data);
	}
	
	

	public function video() {
		$url = trim($this->uri->segment(2));
		$this->data['categories'] = $this->categories_model->getCategories();
		$this->data['latest'] = $this->article->getFrontArticles();
		$this->data['article'] = $this->articles_model->getArticleBySeoUrl($url);
		$this->data['video'] = $this->articles_model->getVideoOfArticle($this->data['article']->id);
		$this->twig->display('default/video.html', $this->data);
	}	

}

