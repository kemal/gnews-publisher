<?php

class Category extends MY_Controller {

    const ITEM_PER_PAGE = 10;

    function __construct() {
        parent::__construct();
        $this->load->model('categories_model');
        $this->load->model('articles_model');
        $this->article = new Articles();
    }

    public function index() {
        $url = trim($this->uri->segment(2));

        $page = $this->getCurrentOffset();
        $this->data['categories'] = $this->categories_model->getCategories();
        $catinfo = $this->categories_model->getCategoryBySeoUrl($url);

        $this->data['category'] = $catinfo;
        $total = $this->articles_model->getArticlesCountByCatID($catinfo->id);
        $this->data['articles'] = $this->article->getArticlesByCatID($catinfo->id, self::ITEM_PER_PAGE, $page);
        $this->data['flash'] = $this->article->getArticlesByPlace('flash', 5);
        $this->data['latest'] = $this->article->getFrontArticles();

        $config = array();
        $config['base_url'] = site_url() . 'category/' . $url;
        $config['total_rows'] = $total;
        $config['per_page'] = self::ITEM_PER_PAGE;
        $config['num_links'] = 3;
        $config['uri_segment'] = 3;
        $this->myPaginationInit($config);
        $this->pagination->initialize($config);
        $this->data['pagination_links'] = $this->pagination->create_links();
        $this->twig->display('default/category.html', $this->data);
    }

    protected function getCurrentOffset() {
        $page = rakam($this->uri->segment(3));
        return !$page ? 0 : $page;
    }

    protected function myPaginationInit(&$config) {
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0)">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li class="next">';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['first_link'] = 'İlk Sayfa';
        $config['last_link'] = 'Son Sayfa';
        $config['prev_link'] = 'Önceki';
        $config['next_link'] = 'Sonraki';
    }

}

