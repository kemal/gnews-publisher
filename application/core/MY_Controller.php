<?php

class MY_Controller extends CI_Controller {

	/**
	 * @var CI_Benchmark
	 */
	public $benchmark;

	/**
	 * @var CI_Calendar
	 */
	public $calendar;

	/**
	 * @var CI_Config
	 */
	public $config;

	/**
	 * @var CI_Email
	 */
	public $email;

	/**
	 * @var CI_Encrypt
	 */
	public $encrypt;

	/**
	 * @var CI_Form_validation
	 */
	public $form_validation;

	/**
	 * @var CI_Ftp
	 */
	public $ftp;

	/**
	 * @var CI_Image_lib
	 */
	public $image_lib;

	/**
	 * @var CI_Input
	 */
	public $input;

	/**
	 * @var CI_Language
	 */
	public $language;

	/**
	 * @var CI_Loader
	 */
	public $load;

	/**
	 * @var CI_Output
	 */
	public $output;

	/**
	 * @var CI_Pagination
	 */
	public $pagination;

	/**
	 * @var CI_Parser
	 */
	public $parser;

	/**
	 * @var CI_Session
	 */
	public $session;

	/**
	 * @var CI_Trackback
	 */
	public $trackback;

	/**
	 * @var CI_Unit_test
	 */
	public $unit;

	/**
	 * @var CI_Upload
	 */
	public $upload;

	/**
	 * @var CI_Uri
	 */
	public $uri;

	/**
	 * @var CI_User_agent
	 */
	public $agent;

	/**
	 * @var CI_Xmlrpc
	 */
	public $xmlrpc;

	/**
	 * @var CI_Xmlrpcs
	 */
	public $xmlrpcs;

	/**
	 * @var CI_Zip
	 */
	public $zip;


	public $data = array();

	function __construct() {

		parent::__construct();

		// twig init
		$this->_twigInit();
		// default form errors
		$this->_formError();
		 
	}


	/**
	 * Twig init
	 */
	protected function _twigInit() {
		// functions
		$this->twig->add_function('relative_time');
		$this->twig->add_function('validation_errors');
		$this->twig->add_function('get_mdate');
		$this->twig->add_function('trim');
		$this->twig->add_function('set_value');

		// global vars
		$this->data['user'] = $this->session->userdata;
		$this->data['site_url'] = base_url();
 
	}

	/**
	 * Form validation default error
	 */
	protected function _formError() {

	}

}